//-----------css-------------------
import '../css/landing.css'
import '../css/landing-md.css'
import '../css/landing-lg.css'
import '../css/landing-extra-lg.css'
//-----------css-------------------


import '../assets/download_links/app store.png'
import '../assets/download_links/app gallery.png'
import '../assets/download_links/google play.png'
import '../assets/phones/main phones/ph1.png'
import '../assets/phones/main phones/ph2.png'

import '../assets/buildings/Academy.png'
import '../assets/chibies/chibi clever.png'


import '../assets/blocks/chibi-characteristics/block primary 1.png'
import '../assets/blocks/chibi-characteristics/block primary 2.png'

import '../assets/chibies/chibi iron.svg'
import '../assets/blocks/battle/sm/elon.png'
import '../assets/blocks/battle/sm/lucy.png'
import '../assets/blocks/battle/sm/block primary.png'

import '../assets/blocks/use radar/primary block tube.svg'
import '../assets/artefacts/use radar/box.png'
import '../assets/artefacts/use radar/sword.svg'
import '../assets/phones/use radar/phone.svg'
import '../assets/blocks/minerals/factory.svg'
import '../assets/blocks/minerals/metall and crystalls.svg'
import '../assets/blocks/minerals/minerals.svg'
import '../assets/blocks/minerals/shield and sword.svg'
import '../assets/phones/transport/p-map.svg'
import '../assets/chibies/chibi moto.svg'
import '../assets/chibies/ninja.svg'
import '../assets/blocks/steal/primary block.png'
import '../assets/blocks/steal/primary warning.svg'
import '../assets/blocks/tokens/sword.svg'
import '../assets/blocks/tokens/marketplace.svg'
import '../assets/phones/token/p-token.svg'

// -----------Head Images--------------
import '../assets/chibies/three chibis.svg'
import '../assets/backgrounds/head-md/bg-head.svg'
import '../assets/decorations/head/head 1/arrow head phones 1.png'
import '../assets/decorations/head/head 1/arrow head phones 2.png'
import '../assets/decorations/head/head 1/vector head phones 1.png'
import '../assets/decorations/head/head 1/vector head phones 2.png'
import '../assets/phones/main phones/phones.svg'
import '../assets/download_links/app store.svg'
import '../assets/download_links/google play.svg'
import '../assets/download_links/soon app gallery.svg'
import '../assets/decorations/head/head 0/md/curve.svg'


//------------Academy-------------------
import '../assets/decorations/academy/cross.svg'
import '../assets/decorations/academy/arrow.svg'
import '../assets/decorations/academy/vector 1.svg'
import '../assets/decorations/academy/arrow 2.svg'
import '../assets/blocks/academy/chibi.svg'
import '../assets/blocks/academy/academy.svg'
import '../assets/decorations/academy/md/arrow spiral.svg'


//----------chibi category---------------
import '../assets/decorations/chibi category/line 1.svg'
import '../assets/decorations/chibi category/line 2.svg'
import '../assets/blocks/chibi-category/md/basic chibi.svg'
import '../assets/blocks/chibi-category/free chibi.svg'
import '../assets/blocks/chibi-category/legendary chibi.svg'
//----------chibi characteristics---------------
import '../assets/decorations/chibi characteristics/arrow.svg'
import '../assets/decorations/chibi characteristics/red arrow.svg'
import '../assets/blocks/chibi-characteristics/characteristics.svg'
import '../assets/decorations/chibi characteristics/md/red arrow 2.svg'

//---------battle---------------------------------
import '../assets/decorations/battle/coin 50.svg'
import '../assets/decorations/battle/coin 100.svg'
import '../assets/decorations/battle/arr 1.svg'
import '../assets/decorations/battle/arr 2.svg'
import '../assets/decorations/battle/bag.svg'
import '../assets/decorations/battle/versus.svg'
import '../assets/blocks/battle/md/elon.svg'
import '../assets/blocks/battle/md/lucy.svg'
import '../assets/decorations/battle/md/curve line.svg'
import '../assets/decorations/battle/md/curve black.svg'

//--------artefacts----------------------------------
import '../assets/decorations/artefacts/spray.svg'
import '../assets/decorations/artefacts/arrow-right.svg'
import '../assets/decorations/artefacts/arrow-left.svg'
import '../assets/blocks/artifacts/radar.svg'
import '../assets/blocks/artifacts/marketplace.svg'
import '../assets/decorations/artefacts/md/curve line red.svg'
//--------radar----------------------------------
import '../assets/decorations/radar/redarr left.svg'
import '../assets/decorations/radar/arrow spiral.svg'
import '../assets/decorations/radar/crown.svg'
import '../assets/decorations/radar/spray.svg'
import '../assets/decorations/radar/md/arrow red.svg'

//--------minerals------------------------
import '../assets/decorations/minerals/arrow red spiral.svg'
import '../assets/decorations/minerals/arrow zig zag.svg'
import '../assets/decorations/minerals/cool artefacts.svg'
import '../assets/decorations/minerals/crown right.svg'
import '../assets/decorations/minerals/rect red.svg'
import '../assets/decorations/minerals/red arrow.svg'
import '../assets/decorations/minerals/info.svg'
import '../assets/decorations/minerals/info-1.svg'

//-----------use radar------------------------
import '../assets/decorations/transport/arr.svg'

//------------steal--------------------------
import '../assets/decorations/steal/arrow red.svg'
import '../assets/decorations/steal/spray black.svg'

//--------tokens-------------------------
import '../assets/decorations/tokens/arrow curve.svg'
import '../assets/decorations/tokens/arrow down.svg'
import '../assets/decorations/tokens/rect 1.svg'
import '../assets/decorations/tokens/rect.svg'
import '../assets/decorations/tokens/spiral.svg'

//---------coming soon---------------------------
import "../assets/decorations/coming soon/arrow zig zag.svg"
import "../assets/decorations/coming soon/spray black.svg"